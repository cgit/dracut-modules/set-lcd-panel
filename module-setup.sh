#!/bin/bash
# -*- mode: shell-script; indent-tabs-mode: nil; sh-basic-offset: 4; -*-
# ex: ts=8 sw=4 sts=4 et filetype=sh

check() {
  return 255
}

depends() {
  return 0
}

installkernel() {
  instmods "=drivers/char/ipmi"
}

install() {
  inst_hook initqueue/settled 99 "$moddir/run_ipmi.sh"
  inst_simple "/usr/sbin/ipmitool"
}
