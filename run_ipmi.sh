#!/bin/bash

if [[ -e "/tmp/ipmi_done" ]] ; then
  exit 0
fi

/usr/sbin/ipmitool delloem lcd set mode userdefined 'Waiting for Key ...'

if [[ "$?" -eq 0 ]] ; then
  touch /tmp/ipmi_done
fi
